package com.vimarket.store;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        SetCalender(context);
    }

    private void SetCalender( Context context)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.add(Calendar.SECOND, 5);
        Intent i = new Intent(context.getApplicationContext(), ScheduleReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(context.getApplicationContext(), 0, i, 0);
        AlarmManager alarm_manager = (AlarmManager)context.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarm_manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1000, pi);
        //Toast.makeText(this, "Service initiated", Toast.LENGTH_SHORT).show();
    }

    public void ProcessDownloading(String _url, Context context)
    {
        Intent i;
        PackageManager manager = context.getPackageManager();
        try
        {

            BootReceiver.DataReceiver d = new BootReceiver.DataReceiver();
            d.setContext(context.getApplicationContext());
            String _result = d.execute(_url).get();
            Log.d("Debug", _result);
            JSONObject j = new JSONObject(_result);

            //get appslist
            JSONArray _array = j.getJSONArray("appList");
            for(int k = 0; k < _array.length(); k++)
            {
                JSONObject _j = _array.getJSONObject(k);
                String _bundle = _j.getString(Constants.BUNDLE_ID_KEY);
                String _appname = _j.getString(Constants.APPLICATION_NAME_KEY);
                String _installPath = Constants.BASE_URL +_j.getString("app_path");
                String _versionName = _j.getString(Constants.VERSION_NAME_KEY);
                int _versionCode = _j.getInt(Constants.VERSION_CODE_KEY);

                //check if bundle id exists
                i = manager.getLaunchIntentForPackage(_bundle);
                if(i == null)
                {
                    //package not installed, dont do anything
                   /* Log.d("Debug", _installPath);
                    InstallAPK downLoadAndInstall = new InstallAPK();
                    downLoadAndInstall.setContext(context.getApplicationContext());
                    downLoadAndInstall.execute(_installPath);*/
                }
                else
                {
                    //check package version
                    PackageInfo p = context.getPackageManager().getPackageInfo(_bundle, 0);
                    if(p.versionName != _versionName)
                    {
                        Log.d("Debug", "Version name is different");
                        if(p.versionCode < _versionCode)
                        {
                            //latest app not there
                            Log.d("Debug", _installPath);
                            InstallAPK downLoadAndInstall = new InstallAPK();
                            downLoadAndInstall.setContext(context.getApplicationContext());
                            downLoadAndInstall.execute(_j.toString());
                        }
                    }

                }

            }

           /* i = manager.getLaunchIntentForPackage("com.ommzi.sardarji2");
            if(i == null)
            {
                Log.d("Debug", "package name not found");
                InstallAPK downLoadAndInstall = new InstallAPK();
                downLoadAndInstall.setContext(context.getApplicationContext());
                downLoadAndInstall.execute(_url);
                return;
            }

            i.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(i);*/

        }
        catch(Exception e)
        {

        }
    }


    private class DataReceiver extends AsyncTask<String, Void, String>
    {
        int status = 0;
        public static final String REQUEST_METHOD = "GET";
        public static final int READ_TIMEOUT = 15000;
        public static final int CONNECTION_TIMEOUT = 15000;
        private Context context;

        String inputLine;
        String result;
        public void setContext(Context context){
            this.context = context;
        }

        public void onPreExecute() {
        }
        @Override
        protected String doInBackground(String... arg0) {

            try {
                URL _u = new URL(arg0[0]);

                HttpURLConnection connection = (HttpURLConnection) _u.openConnection();

                connection.setRequestMethod(REQUEST_METHOD);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);

                connection.connect();

                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();

                while((inputLine = reader.readLine()) != null){
                    stringBuilder.append(inputLine);
                }
                reader.close();
                streamReader.close();
                //Set our result equal to our stringBuilder
                result = stringBuilder.toString();
            }
            catch(IOException e){
                e.printStackTrace();
                result = null;
            }

            return result;
        }
        public void onPostExecute(Void unused) {


        }

    }
}
