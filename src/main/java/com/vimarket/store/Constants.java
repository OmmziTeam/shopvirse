package com.vimarket.store;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.Arrays;

public class Constants
{
    public static String BASE_URL = "http://35.170.107.207:8081/";
    public static String APPLICATION_NAME_KEY = "application_name";
    public static String VERSION_NAME_KEY = "version_name";
    public static String VERSION_CODE_KEY = "version_code";
    public static String BUNDLE_ID_KEY = "bundle_id";
    public static String APP_PATH_KEY = "app_path";

    public static String UNITY_OBJECT_NAME = "Load";
    public static String ERROR = "Error";

    public static int STATUS_SUCCESS = 1;
    public static int STATUS_ERROR = 2;

    public static String INSTALL_TEXT = "INSTALL";
    public static String INSTALLING_TEXT = "INSTALLING";
    public static String UPDATE_TEXT = "UPDATE";
    public static String OPEN_TEXT = "OPEN";
    public static String FORCE_UNINSTALL_AND_INSTALL_KEY = "forceUninstallAndInstall";
    public static String MESSAGE_LOCATION_PUSH = "push";
    public static String MESSAGE_LOCATION_POPUP = "popup";

    public static void createNotification(String message, Context context)
    {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "0")
                .setSmallIcon(R.mipmap.app_icon)
                .setContentTitle("Launch ViRSE")
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, builder.build());
    }

    public static boolean hasApplication(String bundle_id, Context context)
    {
        //return true;
        PackageManager manager = context.getApplicationContext().getPackageManager();
        Intent i = manager.getLaunchIntentForPackage(bundle_id);
        if(i != null)
            return  true;
        else
            return false;
    }

    public static void SaveMessageStatus(String messageId, String messageLocation, Context context)
    {
        //return;
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MyPref", 0);
        SharedPreferences.Editor editor = pref.edit();
        String _data = pref.getString(messageLocation, null);
        if(_data == null || _data == "")
        {
            _data = messageId;
        }
        else
        {
            _data += "," + messageId;
        }
        Log.d("Debug", "Saving " + _data);
        editor.putString(messageLocation, _data);
        editor.commit();
    }

    public static boolean canDisplayMessage(String messageId, String messageLocation, Context context)
    {
        //return true;
        Log.d("Debug", "Inside can display message 0");
        SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MyPref", 0);
        Log.d("Debug", "Inside can display message 1");
        SharedPreferences.Editor editor = pref.edit();
        Log.d("Debug", "Inside can display message 2");
        String _data = pref.getString(messageLocation, null);
        Log.d("Debug", "Inside can display message 3");
        try {
            String[] separated = _data.split("\\,");
            Log.d("Debug", "Inside can display message 4");
            Log.d("Debug", "Found data : " + _data);
            // return true;
            if (Arrays.asList(separated).contains(messageId)) {
                return false;
            } else {
                return true;
            }
        }
        catch(Exception e)
        {
            return true;
        }
    }
}
