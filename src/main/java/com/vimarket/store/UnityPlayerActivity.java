package com.vimarket.store;


import com.unity3d.player.*;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class UnityPlayerActivity extends Activity
{
    public static  UnityPlayerActivity instance;

    protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code

    private Dialog _loader;

    // Setup activity layout
    @Override protected void onCreate(Bundle savedInstanceState)
    {

        if(instance == null)
        {
            instance = this;
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        mUnityPlayer = new UnityPlayer(this);
        setContentView(mUnityPlayer);
        mUnityPlayer.requestFocus();

        if(Build.VERSION.SDK_INT>=24){
            try{
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            }catch(Exception e){
                e.printStackTrace();
            }
        }



        /*BroadcastReceiver OnPackageAdded = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("Debug", "New Package installed UPAAA!!!!!!!!!!!");
            }
        };

        IntentFilter _i = new IntentFilter(Intent.ACTION_PACKAGE_ADDED);
        _i.addDataScheme("package");
        registerReceiver(OnPackageAdded, _i);*/
        //ProgressBar p = (ProgressBar) findViewById(R.id.progressBar1);

        // p.bringToFront();

        _loader = new Dialog(this);
        _loader.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // d.getWindow().setFlags(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        _loader.getWindow().setBackgroundDrawableResource(R.color.transparent);
        _loader.setCancelable(false);
        _loader.setContentView(R.layout.loader);
        _loader.hide();

        //check for message
        getMessage(Constants.BASE_URL + "getMessage", this);


    }

    public void getMessage(String _url, Context context)
    {
        try
        {
            DataReceiver d = new DataReceiver();
            d.setContext(context.getApplicationContext());
            String _result = d.execute(_url).get();
            Log.d("Debug", _result);
            JSONObject j = new JSONObject(_result);
            if(j.has(Constants.ERROR))
            {

            }
            else
            {
                JSONArray _array = j.getJSONArray("messages");
                Log.d("Debug", "Checking application");
                if(Constants.hasApplication(_array.getJSONObject(0).getString(Constants.BUNDLE_ID_KEY), getApplicationContext()))
                { Log.d("Debug", "Found application");
                    if(Constants.canDisplayMessage(_array.getJSONObject(0).getString("id"),Constants.MESSAGE_LOCATION_POPUP, getApplicationContext())) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage(_array.getJSONObject(0).getString("message")).setTitle("Important");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User clicked OK button
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        Log.d("Debug", "Showing alert");
                        Constants.SaveMessageStatus(_array.getJSONObject(0).getString("id"),Constants.MESSAGE_LOCATION_POPUP, getApplicationContext());
                    }

                }



            }


        }
        catch(Exception e)
        {
            Log.d("Error", e.getMessage() );
        }
    }

    public void GetPackageInfo(String packageInfo ) throws JSONException {
        Log.d("Debug", packageInfo);
        PackageManager manager = getApplicationContext().getPackageManager();

        JSONObject _j = new JSONObject(packageInfo);
        String _bundle = _j.getString(Constants.BUNDLE_ID_KEY);

        Intent i = manager.getLaunchIntentForPackage(_bundle);
        Log.d("Debug", "Finding intent...");
        if(i == null)
        {
            Log.d("Debug", "Intent not found");
            mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "OnAppInfoReceived","");
        }
        else
        {
            PackageInfo p = null;
            try
            {
                Log.d("Debug", "Intent found!!!");
                p = getApplicationContext().getPackageManager().getPackageInfo(_bundle, 0);
                JSONObject j = new JSONObject();
                j.put(Constants.APPLICATION_NAME_KEY, p.packageName);
                j.put(Constants.VERSION_NAME_KEY, p.versionName);
                j.put(Constants.VERSION_CODE_KEY, p.versionCode);
                mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "OnAppInfoReceived",j.toString());
            }
            catch (PackageManager.NameNotFoundException e)
            {
                e.printStackTrace();
                mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "OnAppInfoReceived","Error");
            }

        }
    }

    public void UpdatePercentage(boolean show, int percentage,String name) throws JSONException {
            try {
                JSONObject j = new JSONObject();
                j.put("Visible", show);
                j.put("Percentage", percentage);
                j.put("Name", name);

                mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "UpdatePercentage", j.toString());
            }
            catch (Exception e)
            {
                mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "UpdatePercentage", "Error");
            }
    }
    public void GetActiveInstallations()
    {
        if(InstallAPK.instance != null)
        {
            if(InstallAPK.packageInfo != null)
            {
                mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "OnAppInstallationInfo", InstallAPK.packageInfo.toString());
            }
        }
    }

    public void CheckIsDownloading(String appInfo)
    {
        //appinfo must be a json
        try
        {
            JSONObject j = new JSONObject(appInfo);
            CheckDownload downLoadAndInstall = new CheckDownload();
            downLoadAndInstall.setContext(getApplicationContext());
            downLoadAndInstall.execute(j.toString());

        } catch (JSONException e)
        {
            e.printStackTrace();
            mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "OnErrorOnInstallation", appInfo);
        }
    }
    public void InstallOrUpdate(String appInfo)
    {
        //appinfo must be a json
        try
        {
            JSONObject j = new JSONObject(appInfo);
            InstallAPK downLoadAndInstall = new InstallAPK();
            downLoadAndInstall.setContext(getApplicationContext());
            downLoadAndInstall.execute(j.toString());

        } catch (JSONException e)
        {
            e.printStackTrace();
            mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "OnErrorOnInstallation", appInfo);
        }
    }

    public void UninstallApp(String appInfo)
    {
        try {
            JSONObject j = new JSONObject(appInfo);
            Intent intent = new Intent(Intent.ACTION_DELETE);
            intent.setData(Uri.parse("package:" + j.getString(Constants.BUNDLE_ID_KEY)));
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void SetIncompleteInstallation(String appInfo)
    {
        mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "OnErrorOnInstallation", appInfo);
    }

    public void SetCompleteInstallation(String appInfo)
    {
        mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "OnInstallationComplete", appInfo);
    }

    public void OpenApp(String packageInfo)
    {
        Log.d("Debug", "Opening app " + packageInfo);
        try
        {
            JSONObject j = new JSONObject(packageInfo);
            String _bundle = j.getString(Constants.BUNDLE_ID_KEY);
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(_bundle);
            if (launchIntent != null) {
                startActivity(launchIntent);//null pointer check in case package name was not found
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


    }

    private void SetCalender()
    {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        cal.add(Calendar.SECOND, 5);
        Intent i = new Intent(this, ScheduleReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
        AlarmManager alarm_manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarm_manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1000, pi);
        //Toast.makeText(this, "Service initiated", Toast.LENGTH_SHORT).show();
    }

    public void getToken(String _token)
    {
        Log.d("Debug", _token);
        mUnityPlayer.UnitySendMessage(Constants.UNITY_OBJECT_NAME, "OnTokenReceived", _token);
    }

    public void ShowLoader()
    {
        _loader.show();
    }
    public void HideLoader()
    {
        _loader.hide();
    }

    @Override protected void onNewIntent(Intent intent)
    {
        // To support deep linking, we need to make sure that the client can get access to
        // the last sent intent. The clients access this through a JNI api that allows them
        // to get the intent set on launch. To update that after launch we have to manually
        // replace the intent with the one caught here.
        setIntent(intent);
    }

    // Quit Unity
    @Override protected void onDestroy ()
    {
        mUnityPlayer.quit();
        super.onDestroy();
    }

    // Pause Unity
    @Override protected void onPause()
    {
        super.onPause();
        mUnityPlayer.pause();
    }

    // Resume Unity
    @Override protected void onResume()
    {
        super.onResume();
        mUnityPlayer.resume();
    }

    @Override protected void onStart()
    {
        super.onStart();
        mUnityPlayer.start();

        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("Debug", "Started tracker");
                SetCalender();
            }
        }, 1000);


    }

    @Override protected void onStop()
    {
        super.onStop();
        mUnityPlayer.stop();
    }

    // Low Memory Unity
    @Override public void onLowMemory()
    {
        super.onLowMemory();
        mUnityPlayer.lowMemory();
    }

    // Trim Memory Unity
    @Override public void onTrimMemory(int level)
    {
        super.onTrimMemory(level);
        if (level == TRIM_MEMORY_RUNNING_CRITICAL)
        {
            mUnityPlayer.lowMemory();
        }
    }

    // This ensures the layout will be correct.
    @Override public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);

    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override public boolean dispatchKeyEvent(KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override public boolean onKeyUp(int keyCode, KeyEvent event)     { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onKeyDown(int keyCode, KeyEvent event)   { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
    /*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }

    private class DataReceiver extends AsyncTask<String, Void, String>
    {
        int status = 0;
        public static final String REQUEST_METHOD = "GET";
        public static final int READ_TIMEOUT = 15000;
        public static final int CONNECTION_TIMEOUT = 15000;
        private Context context;

        String inputLine;
        String result;
        public void setContext(Context context){
            this.context = context;
        }

        public void onPreExecute() {
        }
        @Override
        protected String doInBackground(String... arg0) {

            try {
                URL _u = new URL(arg0[0]);

                HttpURLConnection connection = (HttpURLConnection) _u.openConnection();

                connection.setRequestMethod(REQUEST_METHOD);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);

                connection.connect();

                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();

                while((inputLine = reader.readLine()) != null){
                    stringBuilder.append(inputLine);
                }
                reader.close();
                streamReader.close();
                //Set our result equal to our stringBuilder
                result = stringBuilder.toString();
            }
            catch(IOException e){
                e.printStackTrace();
                result = null;
            }

            return result;
        }
        public void onPostExecute(Void unused) {


        }

    }
}
