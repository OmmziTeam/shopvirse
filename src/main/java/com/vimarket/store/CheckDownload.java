package com.vimarket.store;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CheckDownload extends AsyncTask<String,Void,Void> {

    int status = 0;

    public static CheckDownload instance;

    private Context context;

    public static JSONObject packageInfo;

    public void setContext(Context context){
        this.context = context;
    }

    public  Cursor downloadingCur;

    public void onPreExecute() {
        instance = this;
    }
    @Override
    protected Void doInBackground(String... arg0)
    {

        try {

            final JSONObject j = new JSONObject(arg0[0]);
            packageInfo = j;


            String _title = j.getString(Constants.APPLICATION_NAME_KEY);
            DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            DownloadManager.Query query = new DownloadManager.Query();
            if(query!=null) {
                query.setFilterByStatus(
                        DownloadManager.STATUS_RUNNING|DownloadManager.STATUS_PENDING);
            } else {
                return  null;
            }
            Log.d("Status",  ":::::::"+ query);

            final Cursor c = downloadManager.query(query);

            c.moveToFirst();
            Log.d("Status",   ":::::::"+ c.getCount());
            final List<String> titleList = new ArrayList<String>();

            for(int i = 0; i < c.getCount(); i++)
            {

                final String title = c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE));
                Log.d("Debug", title + ":::::::"+ _title);
               if(titleList.contains(title))
                    continue;
                titleList.add(title);

                Log.d("Status", title + ":::::::"+ _title);
                if(title.equals(_title)) {
                    downloadingCur = c;
                    // ADD thread
                    new Thread(new Runnable() {

                        @Override
                        public void run() {

                            boolean downloading = true;

                            while (downloading) {

                                if (isCancelled()) break;

                                int bytes_downloaded = downloadingCur.getInt(downloadingCur
                                        .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                                int bytes_total = downloadingCur.getInt(downloadingCur.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                                Log.d("Debug", "downloaded till now " + bytes_downloaded);
                                if (downloadingCur.getInt(downloadingCur.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                                    downloading = false;
                                    try
                                    {
                                        UnityPlayerActivity.instance.UpdatePercentage(true, 100, title);
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }

                                if (downloadingCur.getInt(downloadingCur.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_FAILED ||
                                        downloadingCur.getInt(downloadingCur.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_PAUSED ||
                                        downloadingCur.getInt(downloadingCur.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_FAILED) {
                                    downloading = false;
                                    try {
                                        UnityPlayerActivity.instance.UpdatePercentage(false, 101, "");
                                    }
                                    catch (Exception e)
                                    {

                                    }
                                }

                                final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);
                                Log.d("Debug", "percentage " + dl_progress);
                                try {
                                    UnityPlayerActivity.instance.UpdatePercentage(true, (int) dl_progress, title);
                                }
                                catch (Exception e)
                                {

                                }
                       /* runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                UnityPlayerActivity.instance.UpdatePercentage(true,(int)dl_progress,Constants.APPLICATION_NAME_KEY);


                            }
                        });*/
                            }
                            downloadingCur.close();

                        }
                    }).start();
                }


             //   c.moveToNext();
            }




            return null;
        }
        catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    private boolean checkDownloadStatus(String _title)
    {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Query query = new DownloadManager.Query();
        if(query!=null) {
            query.setFilterByStatus(
                    DownloadManager.STATUS_RUNNING|DownloadManager.STATUS_PENDING);
        } else {
            return false;
        }
        Log.d("Status",  ":::::::"+ query);

        Cursor c = downloadManager.query(query);

        c.moveToFirst();
        Log.d("Status",   ":::::::"+ c.getCount());

        for(int i = 0; i < c.getCount(); i++)
        {

            String title = c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE));


            Log.d("Status", title + ":::::::"+ _title);
            if(title.equals(_title)) {
                downloadingCur = c;
                // ADD thread
                new Thread(new Runnable() {

                    @Override
                    public void run() {

                        boolean downloading = true;

                        while (downloading) {


                            int bytes_downloaded = downloadingCur.getInt(downloadingCur
                                    .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                            int bytes_total = downloadingCur.getInt(downloadingCur.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                            Log.d("Debug", "downloaded till now " + bytes_downloaded);
                            if (downloadingCur.getInt(downloadingCur.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                                downloading = false;
                            }

                            if (downloadingCur.getInt(downloadingCur.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_FAILED) {
                                downloading = false;
                            }

                            final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);
                            Log.d("Debug", "percentage " + dl_progress);
                            try {
                                UnityPlayerActivity.instance.UpdatePercentage(true, (int) dl_progress, packageInfo.getString(Constants.APPLICATION_NAME_KEY));
                            }
                            catch (Exception e)
                            {

                            }
                       /* runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                UnityPlayerActivity.instance.UpdatePercentage(true,(int)dl_progress,Constants.APPLICATION_NAME_KEY);


                            }
                        });*/
                            downloadingCur.close();
                        }

                    }
                }).start();
                return true;
            }


            c.moveToNext();
        }
        return false;


    }
}
