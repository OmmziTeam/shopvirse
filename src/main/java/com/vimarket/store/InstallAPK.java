package com.vimarket.store;

import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;

public class InstallAPK extends AsyncTask<String,Void,Void>
{
    int status = 0;

    public static InstallAPK instance;

    private Context context;

    public static JSONObject packageInfo;

    public void setContext(Context context){
        this.context = context;
    }

    public  Cursor downloadingCur;

    public void onPreExecute() {
        instance = this;
    }
    @Override
    protected Void doInBackground(String... arg0)
    {

        try {
           final JSONObject j = new JSONObject(arg0[0]);
            packageInfo = j;
            String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
            String fileName = j.getString(Constants.APPLICATION_NAME_KEY) + ".apk";
            destination += fileName;
            final Uri uri = Uri.parse("file://" + destination);

            Log.d("Status", "path to save is  " + destination);
            String url = Constants.BASE_URL + j.getString(Constants.APP_PATH_KEY);
            if(checkDownloadStatus(j.getString(Constants.APPLICATION_NAME_KEY)))
            {
                Log.d("Status", "Download should cancel");
                return null;
            }
            else
            {
                Log.d("Status", "Download should resume");
            }

            File file = new File(destination);

            File direct = new File(Environment.getExternalStorageDirectory() + "/Download");

            if (!direct.exists()) {
                File wallpaperDirectory = new File("/sdcard/Download/");
                wallpaperDirectory.mkdirs();
            }

            String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
            Log.d("Status", "Path: " + path);
            File directory = new File(path);
            File[] files = directory.listFiles();
            Log.d("Status", "Size: "+ files.length);
            for (int i = 0; i < files.length; i++)
            {
                Log.d("Status", "FileName:" + files[i].getName());
            }


            //adding broadcaster before
            BroadcastReceiver OnPackageAdded = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    try {
                        ThrowInstallationText(j.getString(Constants.BUNDLE_ID_KEY), context);
                    }
                    catch (Exception e)
                    {}
                }
            };

            IntentFilter _i = new IntentFilter();
            _i.addAction(Intent.ACTION_PACKAGE_ADDED);

            _i.addAction(Intent.ACTION_INSTALL_FAILURE);
            _i.addDataScheme("package");
            context.registerReceiver(OnPackageAdded, _i);


            if (file.isFile()) {
                //file.delete() - test this, I think sometimes it doesnt work
                if(canInstallAPK(destination, j.getInt(Constants.VERSION_CODE_KEY)))
                {
                    Log.d("Status", "installing from sd card");
                    Intent i = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                    i.setDataAndType(Uri.fromFile(new File(destination)), "application/vnd.android.package-archive");
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    Log.d("Status", "Initiating install activity");
                    Log.d("Status", context.getPackageName());
                    Log.d("Status", "package install activity");

                    if(context.getApplicationContext() == null)
                    {
                        Log.d("Status", "Context is null!!");
                    }
                    context.getApplicationContext().startActivity(i);
                    Log.d("Status", "Initiated activity");
                    ThrowInstallationText(j.getString(Constants.BUNDLE_ID_KEY),context);
                    return null;
                }
                else
                {
                    Log.d("Status", "deleting file");
                    file.delete();
                }
            }
            else
            {
                Log.d("Status", "could not find file on path " + destination);
            }


            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
            request.setDescription("Downloading");
            request.setTitle(j.getString(Constants.APPLICATION_NAME_KEY));
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
            request.setDestinationUri(uri);
           // request.setDestinationInExternalFilesDir(context, Environment.DIRECTORY_DOWNLOADS, fileName);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
            request.setVisibleInDownloadsUi(true);
            // get download service and enqueue file
            final DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            final long downloadId = manager.enqueue(request);


            new Thread(new Runnable() {

                @Override
                public void run() {

                    boolean downloading = true;

                    while (downloading) {
                        if (isCancelled()) break;
                        DownloadManager.Query q = new DownloadManager.Query();
                        q.setFilterById(downloadId);



                        Cursor cursor = manager.query(q);
                        cursor.moveToFirst();
                        if(cursor.getCount()==0)
                        {
                            downloading = false;
                            try {
                                UnityPlayerActivity.instance.UpdatePercentage(false, 0, "");
                            }
                            catch (Exception e)
                            {

                            }
                            break;
                        }
                        int bytes_downloaded = cursor.getInt(cursor
                                .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                        int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
                        Log.d("Debug", "downloaded till now " + bytes_downloaded);
                        if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                            downloading = false;
                            try {
                                UnityPlayerActivity.instance.UpdatePercentage(true, 100, cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_TITLE)));
                            }
                            catch (Exception e)
                            {

                            }
                        }
                        if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_FAILED) {
                            downloading = false;
                        }
                        final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);
                        Log.d("Debug", "percentage " + dl_progress);
                            try {
                                UnityPlayerActivity.instance.UpdatePercentage(true, (int) dl_progress, cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_TITLE)));
                            }
                            catch (Exception e)
                            {

                            }
                       /* runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                UnityPlayerActivity.instance.UpdatePercentage(true,(int)dl_progress,Constants.APPLICATION_NAME_KEY);


                            }
                        });*/
                        cursor.close();
                    }

                }
            }).start();

           /* try {
                //Open the specific App Info page:
                Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.parse("package:" + "com.android.providers.downloads"));
                context.startActivity(intent);

            } catch ( ActivityNotFoundException e ) {
                e.printStackTrace();

                //Open the generic Apps page:
                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                context.startActivity(intent);
            }*/




            Log.d("Status", "Started downloading.....");

            BroadcastReceiver onComplete = new BroadcastReceiver() {
                public void onReceive(Context ctxt, Intent intent) {

                    Log.d("Debug", "::::: " + intent.getAction());

                    try {
                        ThrowInstallationText(j.getString(Constants.BUNDLE_ID_KEY), context);
                    }
                    catch (Exception e)
                    {}
                    checkDownloadStatus(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1));

                    try {
                        String destination1 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
                        String fileName1 = packageInfo.getString(Constants.APPLICATION_NAME_KEY) + ".apk";
                        destination1 += fileName1;

                        File saveFile = new File(destination1);

               /* if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    saveFile.setReadable(true, false);
                    Intent intent1 = new Intent(Intent.ACTION_VIEW);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent1.setDataAndType(Uri.fromFile(saveFile), "application/vnd.android.package-archive");
                    context.getApplicationContext().startActivity(intent);
                } else {
                    Intent intent1 = new Intent(Intent.ACTION_VIEW);
                    Uri fileUri = FileProvider.getUriForFile(context,
                            "ir.mhdr.provider",
                            saveFile);

                    intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    context.startActivity(intent);
                }*/

               /* try {
                    final String command = "pm install " + destination1;
                    Process proc = Runtime.getRuntime().exec(new String[] {command });
                    proc.waitFor();
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                        if (Build.VERSION.SDK_INT >= 24) {
                            try {
                                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                                m.invoke(null);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        try {
                            Log.d("Debug", "Reverting back to install text ");
                            UnityPlayerActivity.instance.SetIncompleteInstallation(packageInfo.toString());
                        } catch (Exception e) {
                            Log.d("Debug", e.getMessage());
                            e.printStackTrace();
                        }
                        Log.d("Debug", "staring activity ");
                        Intent i = new Intent(Intent.ACTION_INSTALL_PACKAGE);

                        i.setDataAndType(Uri.fromFile(new File(destination1)), "application/vnd.android.package-archive");
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        context.startActivity(i);
                        return;
                    }
                    catch(JSONException e)
                    {
                        return;
                    }
                /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    Intent install = new Intent(Intent.ACTION_VIEW);
                    install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    install.setDataAndType(uri,
                            manager.getMimeTypeForDownloadedFile(downloadId));
                    context.startActivity(install);
                }
                else
                {
                    Intent intent1 = new Intent(Intent.ACTION_VIEW);
                    Uri fileUri = FileProvider.getUriForFile(context,
                            "ir.mhdr.provider",
                            saveFile);

                    intent.setDataAndType(fileUri, "application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    context.startActivity(intent);
                }*/

                    // context.unregisterReceiver(this);

                }
            };



            BroadcastReceiver OnDownloadCancel = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    try {
                        Log.d("Debug", intent.getAction());
                        Log.d("Debug", "New Package installed!!!!!!!!!!! " + packageInfo.getString(Constants.BUNDLE_ID_KEY));

                        UnityPlayerActivity.instance.SetIncompleteInstallation(packageInfo.toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                }
            };



            //register receiver for when .apk download is compete
            context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            context.registerReceiver(OnDownloadCancel, new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED));






            return null;



        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }


    }

    private void ThrowInstallationText(String bundleId, Context c)
    {
        if(Constants.hasApplication(bundleId, c))
        {
            try {
                UnityPlayerActivity.instance.SetCompleteInstallation(packageInfo.toString());
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
        else
        {
            try {
                UnityPlayerActivity.instance.SetIncompleteInstallation(packageInfo.toString());
            } catch (Exception e) {

                e.printStackTrace();
            }
        }

    }
    private boolean isPackageInstalled(String bundleId, Context c)
    {
        if(Constants.hasApplication(bundleId, c))
            return true;
        else
            return false;
    }
    private void checkDownloadStatus(long downloadReference) {
        DownloadManager.Query myDownloadQuery = new DownloadManager.Query();
        myDownloadQuery.setFilterById(downloadReference);
        DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        Cursor cursor = manager.query(myDownloadQuery);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
            int status = cursor.getInt(columnIndex);
            //column for reason code if the download failed or paused
            int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
            int reason = cursor.getInt(columnReason);
            //get the download filename

            String statusText = "";
            String reasonText = "";

            switch (status) {
                case DownloadManager.STATUS_FAILED:
                    try {

                        UnityPlayerActivity.instance.SetIncompleteInstallation(packageInfo.toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    break;
                case DownloadManager.STATUS_PAUSED:
                    try {

                        UnityPlayerActivity.instance.SetIncompleteInstallation(packageInfo.toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    break;
                case DownloadManager.STATUS_PENDING:
                    statusText = "STATUS_PENDING";
                    break;
                case DownloadManager.STATUS_RUNNING:
                    statusText = "STATUS_RUNNING";
                    break;
                case DownloadManager.STATUS_SUCCESSFUL:
                    statusText = "STATUS_SUCCESSFUL";
                    try {

                        UnityPlayerActivity.instance.SetIncompleteInstallation(packageInfo.toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    break;
            }
        }

    }



//not in use
    protected Void doInBackground1(String... arg0) {
        try {
            /*URL url = new URL(arg0[0]);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            File sdcard = Environment.getExternalStorageDirectory();
            File myDir = new File(sdcard,"Android/data/com.vimarket.store/temp");
            myDir.mkdirs();
            File outputFile = new File(myDir, "temp.apk");
            if(outputFile.exists()){
                outputFile.delete();
            }
            FileOutputStream fos = new FileOutputStream(outputFile);

            InputStream is = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
            }
            fos.flush();
            fos.close();
            is.close();*/

            URL u = new URL(arg0[0]);
            URLConnection conn = u.openConnection();
            int contentLength = conn.getContentLength();

            DataInputStream stream = new DataInputStream(u.openStream());



            byte[] buffer = new byte[contentLength];
            long total = 0;
            int count;
           /* while ((count = stream.read(buffer)) != -1) {
                // allow canceling with back button

                total += count;
                // publishing the progress....
                if (contentLength > 0) // only if total length is known
                    Log.d("Debug", String.valueOf(((int) (total * 100 / contentLength))));

            }*/
            Log.d("Debug", "1");
            stream.readFully(buffer);

            Log.d("Debug", "2");
            stream.close();
            Log.d("Debug", "3 " + Environment.getExternalStorageDirectory()+"/Sardarji");
            DataOutputStream fos = new DataOutputStream(new FileOutputStream(Environment.getExternalStorageDirectory()+"/Android/data/com.vimarket.store/temp/Sardarji.apk"));
            Log.d("Debug", "4");
            fos.write(buffer);
            fos.flush();
            fos.close();
            Log.d("Debug", "1");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            //intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory(),"Android/data/com.mycompany.android.games/temp/temp.apk")), "application/vnd.android.package-archive");

            Uri apkURI = FileProvider.getUriForFile(
                    context,
                    context.getApplicationContext()
                            .getPackageName() + ".provider", new File(Environment.getExternalStorageDirectory(),"Android/data/com.vimarket.store/temp/Sardarji.apk"));
            intent.setDataAndType(apkURI, "application/vnd.android.package-archive");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
            context.startActivity(intent);


        } catch (FileNotFoundException fnfe) {
            status = 1;
            Log.e("File", "FileNotFoundException! " + fnfe);
        }

        catch(Exception e)
        {
            Log.e("UpdateAPP", "Exception " + e);
        }
        return null;
    }

    public void onPostExecute(Void unused) {

        if(status == 1)
            Toast.makeText(context,"App Not Available",Toast.LENGTH_LONG).show();
        else
        {
            //Toast.makeText(context, "balle balle balle", Toast.LENGTH_LONG).show();
            /*try
            {
                UnityPlayerActivity.instance.SetCompleteInstallation(packageInfo.toString());
            }
            catch (Exception e)
            {}*/
        }
    }

    private boolean checkDownloadStatus(String _title)
    {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Query query = new DownloadManager.Query();
        if(query!=null) {
            query.setFilterByStatus(
                    DownloadManager.STATUS_RUNNING|DownloadManager.STATUS_PENDING);
        } else {
            return false;
        }
        Cursor c = downloadManager.query(query);
        c.moveToFirst();
        for(int i = 0; i < c.getCount(); i++)
        {

            String title = c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE));


                Log.d("Status", title + ":::::::"+ _title);
                if(title.equals(_title)) {
                    return true;
                }


            c.moveToNext();
        }
        return false;


    }

    private boolean canInstallAPK(String path, int targetVersionCode)
    {
        PackageManager pm = context.getPackageManager();
        PackageInfo info = pm.getPackageArchiveInfo(path, 0);
        try {
            Log.d("Status" , "found version code " + info.versionCode + " required " + targetVersionCode);
            if (info.versionCode == targetVersionCode) {
                return true;
            } else
                return false;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }


}