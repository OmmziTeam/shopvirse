package com.vimarket.store;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import android.app.DownloadManager;


public class ScheduleReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //Toast.makeText(context, "Service initiated", Toast.LENGTH_SHORT).show();
        ProcessDownloading(Constants.BASE_URL + "getApps", context);
        getMessage(Constants.BASE_URL + "getMessage", context);
    //throw new UnsupportedOperationException("Not yet implemented");
}

    public void getMessage(String _url, Context context)
    {
        try
        {
            DataReceiver d = new DataReceiver();
            d.setContext(context.getApplicationContext());
            String _result = d.execute(_url).get();
            Log.d("Status", _result);
            JSONObject j = new JSONObject(_result);
            if(j.has(Constants.ERROR))
            {

            }
            else
            {
                JSONArray _array = j.getJSONArray("messages");

                DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Query query = new DownloadManager.Query();
                if(query!=null) {
                    //alreday downloading.
                    return;
                }

                for(int k = 0; k < _array.length(); k++)
                {
                    final JSONObject _j = _array.getJSONObject(k);
                    String b_id = _j.getString(Constants.BUNDLE_ID_KEY);


                    if(Constants.hasApplication(b_id, context)) {
                        if(Constants.canDisplayMessage(_array.getJSONObject(k).getString("id"),Constants.MESSAGE_LOCATION_PUSH, context)) {
                            Constants.createNotification(_j.getString("message"), context);
                        }
                    }
                }
            }

        }
        catch(Exception e)
        {
            Log.d("Error", e.getMessage() );
        }
    }

    public void ProcessDownloading(String _url, Context context)
    {
        Intent i;
        PackageManager manager = context.getPackageManager();
        try
        {

            DataReceiver d = new DataReceiver();
            d.setContext(context.getApplicationContext());
            String _result = d.execute(_url).get();
            Log.d("Status", _result);
            JSONObject j = new JSONObject(_result);

            //get appslist
            JSONArray _array = j.getJSONArray("appList");
            for(int k = 0; k < _array.length(); k++)
            {
                final JSONObject _j = _array.getJSONObject(k);
                final String _bundle = _j.getString(Constants.BUNDLE_ID_KEY);
                String _appname = _j.getString(Constants.APPLICATION_NAME_KEY);
                String _installPath = Constants.BASE_URL +_j.getString("app_path");
                String _versionName = _j.getString(Constants.VERSION_NAME_KEY);
                int _versionCode = _j.getInt(Constants.VERSION_CODE_KEY);
                int _forceUpdate = _j.getInt(Constants.FORCE_UNINSTALL_AND_INSTALL_KEY);

                //check if bundle id exists
                i = manager.getLaunchIntentForPackage(_bundle);
                if(i == null)
                {
                    //package not installed, dont do anything
                   /* Log.d("Status", _installPath);
                    InstallAPK downLoadAndInstall = new InstallAPK();
                    downLoadAndInstall.setContext(context.getApplicationContext());
                    downLoadAndInstall.execute(_installPath);*/
                }
                else
                {
                    if(_forceUpdate == 1)
                    {
                        PackageInfo p = context.getPackageManager().getPackageInfo(_bundle, 0);
                        Log.d("Status", p.versionCode + " " + _versionCode );
                        if (p.versionCode < _versionCode) {
                            BroadcastReceiver OnUninstall = new BroadcastReceiver() {
                                @Override
                                public void onReceive(Context context, Intent intent) {
                                    Log.d("Status", intent.getDataString() + "<><><><<><><" );
                                    InstallAPK downLoadAndInstall = new InstallAPK();
                                    downLoadAndInstall.setContext(context.getApplicationContext());
                                    downLoadAndInstall.execute(_j.toString());
                                }
                            };
                            Log.d("Status", "uninstallation initiated 0" );
                            IntentFilter _i = new IntentFilter();
                            _i.addAction(Intent.ACTION_PACKAGE_REMOVED);
                            _i.addDataScheme("package");
                            context.getApplicationContext().registerReceiver(OnUninstall, _i);

                            Log.d("Status", "uninstallation initiated 0-1" );
                            Intent intent = new Intent(Intent.ACTION_DELETE);
                            intent.setData(Uri.parse("package:" + _bundle));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            Log.d("Status", "uninstallation initiated" );
                        }

                    }
                    else {
                        //check package version

                        PackageInfo p = context.getPackageManager().getPackageInfo(_bundle, 0);


                        if (p.versionName != _versionName) {
                            Log.d("Status", "Version name is different version code of package:" +p.versionCode + " server found " + _versionCode );
                            if (p.versionCode < _versionCode) {
                                //latest app not there
                                Log.d("Status", _installPath);
                                InstallAPK downLoadAndInstall = new InstallAPK();
                                downLoadAndInstall.setContext(context.getApplicationContext());
                                downLoadAndInstall.execute(_j.toString());
                            }
                        }
                    }

                }

            }

           /* i = manager.getLaunchIntentForPackage("com.ommzi.sardarji2");
            if(i == null)
            {
                Log.d("Status", "package name not found");
                InstallAPK downLoadAndInstall = new InstallAPK();
                downLoadAndInstall.setContext(context.getApplicationContext());
                downLoadAndInstall.execute(_url);
                return;
            }

            i.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(i);*/

        }
        catch(Exception e)
        {
            Log.d("Error", e.getMessage() );
        }
    }


    private class DataReceiver extends AsyncTask<String, Void, String>
    {
        int status = 0;
        public static final String REQUEST_METHOD = "GET";
        public static final int READ_TIMEOUT = 15000;
        public static final int CONNECTION_TIMEOUT = 15000;
        private Context context;

        String inputLine;
        String result;
        public void setContext(Context context){
            this.context = context;
        }

        public void onPreExecute() {
        }
        @Override
        protected String doInBackground(String... arg0) {

            try {
                URL _u = new URL(arg0[0]);

                HttpURLConnection connection = (HttpURLConnection) _u.openConnection();

                connection.setRequestMethod(REQUEST_METHOD);
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);

                connection.connect();

                InputStreamReader streamReader = new
                        InputStreamReader(connection.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);
                StringBuilder stringBuilder = new StringBuilder();

                while((inputLine = reader.readLine()) != null){
                    stringBuilder.append(inputLine);
                }
                reader.close();
                streamReader.close();
                //Set our result equal to our stringBuilder
                result = stringBuilder.toString();
            }
            catch(IOException e){
                e.printStackTrace();
                result = null;
            }

            return result;
        }
        public void onPostExecute(Void unused) {


        }

    }

}


